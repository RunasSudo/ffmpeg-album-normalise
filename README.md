# ffmpeg-album-normalise

Normalise music on an album-by-album basis using FFmpeg loudnorm, à la album ReplayGain

This project has moved to https://yingtongli.me/git/ffmpeg-album-normalise/
